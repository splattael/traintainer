require "gitlab"
require "parallel"

module Traintainer
  Projects = {
    ce: 13083,
    ee: 278964
  }

  Emoji = "eyes"

  Template = <<~MARKDOWN
    ### %{title}: %{full_url}
    %{authored_by_me}
    During review:

    - (List anything of note, or a quick summary. "I suggested/identified/noted...")

    Post-review:

    - (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

  MARKDOWN

  TemplateAuthoredByMe = <<~MARKDOWN

    :selfie: *Authored by me*
  MARKDOWN

  TemplateFooter = <<~MARKDOWN
    @%{merger} Please add feedback, and compare this review to the average maintainer review.
  MARKDOWN

  ShowTemplateFooterWithin = 14 * 86400 # 2 weeks

  def self.template_for(mr)
    authored_by_me = TemplateAuthoredByMe if mr.author.id == UserId

    template = Template % {
      title: mr.title,
      full_url: mr.web_url,
      authored_by_me: authored_by_me
    }

    merged_ago = Time.now - Time.parse(mr.merged_at)

    if merged_ago <= ShowTemplateFooterWithin
      template << TemplateFooter % { merger: mr.merged_by.username }
      template << "\n"
    end

    template
  end

  def self.header(mr)
    separator = "=" * 80
    merged_at = Time.parse(mr.merged_at).strftime("%Y-%m-%d")

    <<~HEADER
      #{separator}
      Title      #{mr.title}
      URL        #{mr.web_url}
      Author     @#{mr.author.username}
      Merged at  #{merged_at} (%#{mr.milestone&.title}) by @#{mr.merged_by.username}
      User notes #{mr.user_notes_count}
      #{separator}

    HEADER
  end

  UserId = Gitlab.user.id
end
