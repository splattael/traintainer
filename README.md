# Traintainer

## Setup

```bash
bundle
export GITLAB_API_ENDPOINT=https://gitlab.com/api/v4
export GITLAB_API_PRIVATE_TOKEN=glpat--8GMtG8Mf4EnM***
```

## Mark relevant MRs

Mark all participating MRs with :eyes: (`:eyes:`) for all CE and EE project
since `StartDate`.

:warning: This can take a long time.

Verify [its progress](https://gitlab.com/dashboard/merge_requests?my_reaction_emoji=eyes).

```bash
ruby mark_mrs
```

## Create comments in your traintainer issue

```bash
ruby create_comments

================================================================================
Title      Some title
URL        https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/12345
Author     @some_author
Merged at  2019-05-23 (%12.3) by @some_merger
User notes 42
================================================================================

### Some title: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/12345

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")o

@some_merger Please add feedback, and compare this review to the average maintainer review.

>>> Unmark MR? (y/N)
```

Copy+paste the generate template snippet into your traintainer issue.

Add items at will. Unmark this MR by entering `y` :)

Next.
